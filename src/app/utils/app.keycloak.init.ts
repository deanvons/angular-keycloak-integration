import { KeycloakService } from "keycloak-angular";

export function initializeKeycloak(keycloak: KeycloakService):()=>Promise<boolean> {
    return () =>
      keycloak.init({
        config: {
          url: '<<keycloak-instance-url>>/auth',
          realm: '<<realm-name>>',
          clientId: '<<client-id>>',
        },
        initOptions: {
            checkLoginIframe:true,
            checkLoginIframeInterval:25
        //   onLoad: 'check-sso',
        //   silentCheckSsoRedirectUri:
        //     window.location.origin + '/assets/silent-check-sso.html',
        },
        loadUserProfileAtStartUp:true
      });
  }
  