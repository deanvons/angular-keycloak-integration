import { NgModule, APP_INITIALIZER } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import {initializeKeycloak} from './utils/app.keycloak.init'

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SecretAreaComponent } from './secret-area/secret-area.component';
import { HomePageComponent } from './home-page/home-page.component';

@NgModule({
	declarations: [AppComponent, SecretAreaComponent, HomePageComponent],
	imports: [BrowserModule, AppRoutingModule, KeycloakAngularModule],
	providers: [
		{
			provide: APP_INITIALIZER,
			useFactory: initializeKeycloak,
			multi: true,
			deps: [KeycloakService],
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
