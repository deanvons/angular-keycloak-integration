import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { HomePageComponent } from "./home-page/home-page.component";
import { SecretAreaComponent } from "./secret-area/secret-area.component";

const routes: Routes = [
	{
    path:'',
    redirectTo: 'home',
    pathMatch:'full'
  },
  {
    path:'home',
    component:HomePageComponent
  },
	{
		path: "secret",
		component: SecretAreaComponent,
		canActivate: [AuthGuard],
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
