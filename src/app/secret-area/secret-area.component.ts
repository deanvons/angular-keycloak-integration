import { Component, OnInit } from "@angular/core";
import { KeycloakService } from "keycloak-angular";
import { KeycloakProfile } from "keycloak-js";

@Component({
	selector: "app-secret-area",
	templateUrl: "./secret-area.component.html",
	styleUrls: ["./secret-area.component.css"],
})
export class SecretAreaComponent implements OnInit {

  userName?= ''
  firstName?= ''
  lastName?= ''
  email?=''
  token?=''
  roles:string[]=[]
	constructor(private keycloakService: KeycloakService) {}

	ngOnInit(): void {
		this.loadUserInfo();
	}

	public logout(): void {
    localStorage.clear();
		this.keycloakService.logout("http://localhost:4200");
	}

	public loadUserInfo() {
    this.keycloakService.getToken()
    .then(token=> localStorage.setItem("Access Token", token))

    this.keycloakService.loadUserProfile()
    .then(kcProfile=>{
      this.userName = kcProfile.username
      this.firstName = kcProfile.firstName
      this.lastName = kcProfile.lastName
      this.email = kcProfile.email
    }
      )

      this.roles = this.keycloakService.getUserRoles(true)
  }
}
