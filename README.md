# AngularKeycloak

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.2.

## Keycloak configuration

Add keycloak configuration in /utils/app.keycloak.init.ts

```Javascript
     config: {
          url: '<<keycloak-instance-url>>/auth',
          realm: '<<realm-name>>',
          clientId: '<<client-id>>',
        },
```
Click 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Use

Clicking on the 'Go to super secret area' button will redirect the user to login to the keycloak instance specified above. 

Once logged in the token will stored in local storage and the user will be redirected to a page where some of the users information is displayed

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
